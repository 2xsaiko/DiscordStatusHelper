use std::time::Duration;
use std::thread::sleep;
use std::process::Command;
use std::env;

fn main() {
  let args = env::args().collect::<Vec<_>>();
  let pid: Option<u32> = {
    let arg1 = args.get(1);
    match arg1 {
      Some(s) => Some(s.parse::<u32>().expect(&format!("Could not parse {} as a PID!", s))),
      None => None
    }
  };

  match pid {
    Some(p) => println!("Tracking PID {}.", p),
    None => println!("No PID passed, looping infinitely.")
  }

  let dur = Duration::from_secs(5);
  loop {
    if pid.is_some() && !is_process_alive(pid.unwrap()) {
      println!("Parent process (PID {}) died, stopping...", pid.unwrap());
      break;
    }
    sleep(dur);
  }
}

fn is_process_alive(pid: u32) -> bool {
  let result = Command::new("/usr/bin/env")
    .args(vec!["kill", "-0", &pid.to_string()])
    .status()
    .expect("Failed to spawn process?? Are we running on Linux?");

  result.success()
}