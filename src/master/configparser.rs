use toml::Value;
use toml::value::Table;
use conditionlogic::{Rules, Rule, ConditionGroup, Condition};
use std::fs::File;
use std::io::Read;
use std::collections::HashSet;

pub fn from_file(path: &str) -> Rules {
  let mut file = File::open(path).unwrap();
  let file_len = file.metadata().unwrap().len();
  let mut v: Vec<u8> = Vec::with_capacity(file_len as usize + 1);
  file.read_to_end(&mut v).expect("Unable to read file");
  let s = String::from_utf8(v).unwrap();
  parse(&s)
}

pub fn parse(text: &str) -> Rules {
  let v = text.parse::<Value>().unwrap();
  let mut vec = Vec::new();
  for el in v["entry"].as_array().unwrap() {
    vec.push(parse_rule(el.as_table().unwrap()));
  }
  let mut rules = Rules::new(&vec);
  rules.sort();
  rules
}

fn parse_rule(rule: &Table) -> Rule {
  let name = rule["name"].as_str().unwrap().to_string();
  let priority = rule["priority"].as_integer().unwrap_or(0);
  let mut vec = Vec::new();
  if rule.contains_key("condition") {
    for el in rule["condition"].as_array().unwrap() {
      vec.push(parse_condition(el.as_table().unwrap()));
    }
  }
  Rule::new(name, priority as i16, group_conditions(vec))
}

fn parse_condition(condition: &Table) -> Condition {
  let field = condition["field"].as_str().unwrap();
  let expr = condition["expr"].as_str().unwrap();
  let group = if condition.contains_key("group") { condition["group"].as_integer().map(|x| x as u16) } else { None };
  Condition::new(field, expr, group)
}

fn group_conditions(conditions: Vec<Condition>) -> Vec<ConditionGroup> {
  let mut vec = Vec::new();

  conditions.clone().into_iter()
    .filter(|el| el.group.is_none())
    .map(|el| ConditionGroup::new(vec![el]))
    .for_each(|el| vec.push(el));

  conditions.iter()
    .filter(|el| el.group.is_some())
    .map(|el| el.group.unwrap())
    .collect::<HashSet<_>>().into_iter()
    .map(|el| ConditionGroup::new(conditions.clone().into_iter().filter(|x| x.group.is_some() && x.group.unwrap() == el).collect()))
    .for_each(|el| vec.push(el));

  vec
}

// debug
// fn print_map<K: Display, V: Display>(map: &BTreeMap<K, V>) {
//   println!("{{");
//   for (k, v) in map {
//     println!("  {}: {}", k, v);
//   }
//   println!("}}")
// }
//
// fn print_vec<E: Display>(vec: &Vec<E>) {
//   println!("[");
//   for e in vec {
//     println!("{}", e);
//   }
//   println!("]");
// }