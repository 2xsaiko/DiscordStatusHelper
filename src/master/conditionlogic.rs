use std::collections::HashMap;
use regex::Regex;

trait TCondition {
  fn matches(&self, props: &HashMap<String, String>) -> bool;
}

#[derive(Clone)]
pub struct Rules {
  rules: Vec<Rule>,
}

impl Rules {
  pub fn new<'b>(rules: &'b Vec<Rule>) -> Rules {
    Rules {
      rules: rules.clone()
    }
  }

  pub fn sort(&mut self) {
    self.rules.sort_by(|a, b| b.priority.cmp(&a.priority));
  }

  fn sort_rules(rules: &mut Vec<&Rule>) {
    rules.sort_by(|a, b| b.priority.cmp(&a.priority));
  }

  pub fn entry_matches(&self, props: &HashMap<String, String>) -> Option<&Rule> {
    for rule in self.rules.iter() {
      if rule.matches(props) {
        return Some(rule);
      }
    }

    None
  }

  pub fn entry_matches_all(&self, props: &Vec<HashMap<String, String>>) -> Option<&Rule> {
    let mut options = Vec::new();
    for w in props {
      match self.entry_matches(w) {
        Some(rule) => options.push(rule),
        None => ()
      }
    }
    Rules::sort_rules(&mut options);
    options.first().map(|x| *x)
  }
}

#[derive(Clone)]
pub struct Rule {
  pub name: String,
  priority: i16,
  conditions: Vec<ConditionGroup>,
}

impl Rule {
  pub fn new(name: String, priority: i16, conditions: Vec<ConditionGroup>) -> Rule {
    Rule { name, priority, conditions }
  }
}

impl TCondition for Rule {
  fn matches(&self, props: &HashMap<String, String>) -> bool {
    self.conditions.iter()
      .all(|x| x.matches(props))
  }
}

#[derive(Clone)]
pub struct ConditionGroup {
  components: Vec<Condition>,
}

impl ConditionGroup {
  pub fn new(components: Vec<Condition>) -> ConditionGroup {
    ConditionGroup { components }
  }
}

impl TCondition for ConditionGroup {
  fn matches(&self, props: &HashMap<String, String>) -> bool {
    self.components.iter()
      .any(|x| x.matches(props))
  }
}

#[derive(Clone)]
pub struct Condition {
  pub field: String,
  pub expr: String,
  pub group: Option<u16>,
}

impl Condition {
  pub fn new(field: &str, expr: &str, group: Option<u16>) -> Condition {
    Condition {
      field: field.to_string(),
      expr: expr.to_string(),
      group
    }
  }
}

impl TCondition for Condition {
  fn matches(&self, props: &HashMap<String, String>) -> bool {
    let field_contents = match props.get(&self.field) {
      Some(s) => s,
      None => ""
    };

    match Regex::new(&self.expr) {
      Ok(r) => {
        r.is_match(field_contents)
      }
      Err(_) => false
    }
  }
}